#!/bin/bash
env="$1"
echo yor env is $env
echo "delete ns $env"
./loop
echo "ns $env has been deleted"
if [[ "$env" = "prod" ]]; then
  pv='data2'
else
  pv='data'
fi
echo "rm /$pv/*"
rm -rf '/$pv/*'
echo "/$pv has been cleaned"
echo "delete pvc_$env, pv_$env"
kubectl delete -f pvc_$env.yaml
kubectl delete -f pv_$env.yaml
echo "pv, pvc $env has been deleted"
echo "create pv,pvc $env"
kubectl apply -f pv_$env.yaml
kubectl apply -f pvc_$env.yaml
echo "pv,pvc $env has been created"
